#include <iostream>
#include "factorial.h"

using namespace std;

int main() {
    int n;

    cout << "Enter a number to calculate the factorial: ";
    cin >> n;

    if (cin.fail()) {
        cerr << "Error: Invalid input.\n";
        return 1;
    }

    unsigned long long result = factorial(n);
    if (result != 0) {
        cout << "Factorial numbers " << n << " equal to " << result << endl;
    }

    return 0;
}
